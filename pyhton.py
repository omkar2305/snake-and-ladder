import random
dice=[1,2,3,4,5,6]     # A list of numbers from 1 to 6 to make a dice.

def ludo_start():
    '''
    This function is to start the game if and only if the dice rolls 6. 
    If it doesn't give 6, it will ask to roll again.
    '''
    l=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]     # List l represents the board of 20 steps.
    board=[]
    while True:
        r=random.choice(dice)                                  # It randomly selects an item from the dice variable.
        roll=input("Roll (Press Enter): ")                     # which works as dice.
        if r==6:
            print('Dice: ',r)
            p='| "Start" |'
            print(' _________')
            print(p)
            print('---------------------------------')
            break
        else:
            print('Dice: ',r)

def ludo_game():
    '''
    This function starts the game and adds the number of steps on the board
    with respect to the value of dice we get after rolling.
    As the board .i.e. our list l reaches upto 20, the game stops. 
    '''
    l=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    board=[]
    while True:        
        roll=input("Roll (Press Enter): ")
        r=random.choice(dice)
        if len(board) >= 15 and r>5:                       # If the board requires <= 5 steps to complete,
            print(r)                                       # It will not accept 6 as the value of dice
            print('Roll Again')                            # And it will ask to roll again
            print('---------------------------------')
            continue
        elif len(board) >= 16 and r>4:                       # If the board requires <= 4 steps to complete,
            print(r)                                       # It will not accept greater than 4 as the value of dice
            print('Roll Again')
            print('---------------------------------')
            continue
        elif len(board) >= 17 and r>3:                       # If the board requires <= 3 steps to complete,
            print(r)                                       # It will not accept greater than 3 as the value of dice
            print('Roll Again')
            print('---------------------------------')
            continue
        elif len(board) >= 18 and r>2:                       # If the board requires <= 2 steps to complete,
            print(r)                                       # It will not accept greater than 2 as the value of dice
            print('Roll Again')
            print('---------------------------------')
            continue
        elif len(board) >= 19 and r>1:                       # If the board requires <= 1 steps to complete,
            print(r)                                       # It will not accept greater than 1 as the value of dice
            print('Roll Again')
            print('---------------------------------')
            continue
        print('Dice: ',r)
        board=board+l[:r]
        
        '''
        The code below is for ladder and Snake. 
        '''
        
        if len(board)==3:                           #Ladder - If the player lands on 3, it will climb upto 13.
            print("Board: ",board)
            del l[:r]
#            print('++++',l)
            print(' __________')
            print('| "Ladder" |')
            board=board+l[:10]
            print("Board: ",board)
            del l[:10]
#            print('++++',l)
            print('---------------------------------')
        elif len(board)==7:                         #Ladder - If the player lands on 7, it will climb upto 15.
            print("Board: ",board)
            del l[:r]
#            print('++++',l)
            print(' __________')
            print('| "Ladder" |')
            board=board+l[:8]
            print("Board: ",board)
            del l[:8]
#            print('++++',l)
            print('---------------------------------')    
        elif len(board)==12:                        #Snake - If the player lands on 12, it will slide down to 4.
            print(board)
            del l[:r]
            l=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
            print(' _________')
            print('| "Snake" |')
            board=[]
            board=board+l[:4]
            print("Board: ",board)
            del l[:4]
            print('---------------------------------')
        elif len(board)==14:                        #Snake - If the player lands on 14, it will slide down to 6.
            print("Board: ",board)
            del l[:r]
            l=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
            print(' _________')
            print('| "Snake" |')
            board=[]
            board=board+l[:6]
            print("Board: ",board)
            del l[:6]
            print('---------------------------------')
        elif len(board)==18:                        #Snake - If the player lands on 18, it will slide down to 2.
            print(board)
            del l[:r]
            l=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
            print(' _________')
            print('| "Snake" |')
            board=[]
            board=board+l[:2]
            print("Board: ",board)
            del l[:2]
            print('---------------------------------')
        else:
            print("Board: ",board)
            del l[:r]
#            print('++++',l)
            print('---------------------------------')        
        if len(board)==20:                          #As the length of board is equal to 20, game finishes
            print("|----+++ GREAT +++----|")
            print('---------------------------------')  
            break
            
ludo_start()
ludo_game()